import React from 'react'

class Results extends React.Component {
  render() {
    return (
      <div className="results">
        <div className="card">
          <div className="card-header">
            Results
          </div>
          <div className="card-text">
          </div>
        </div>
        <div className="card">
          <div className="card-text">
            <b>Numbers 1:</b>
            <ul>
              <li>average: {this.props.results.numbers_1.average}</li>
              <li>minimum: {this.props.results.numbers_1.minimum}</li>
              <li>median: {this.props.results.numbers_1.median}</li>
              <li>outlers: {this.props.results.numbers_1.outlier.toString(', ')}</li>
            </ul>
          </div>
          <div className="card-text">
            <b>Numbers 2:</b>
            <ul>
              <li>average: {this.props.results.numbers_2.average}</li>
              <li>minimum: {this.props.results.numbers_2.minimum}</li>
              <li>median: {this.props.results.numbers_2.median}</li>
              <li>outlers: {this.props.results.numbers_2.outlier.toString(', ')}</li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Results
