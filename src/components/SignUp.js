import React from 'react';
import SignUpForm from './../forms/SignUpForm'
import { onFormSubmit } from './../store/signUp/actions'
import { Redirect } from 'react-router';

class SignUp extends React.Component {
  submit = (values, dispatch) => { dispatch(onFormSubmit(values)) }

  render() {
    if (this.props.currentUser) {
      return (<Redirect to={'/'} />)
    } else {
      return (<SignUpForm onSubmit={this.submit} />)
    }

  }
}

export default SignUp;


