import React from 'react';
import AnaluzerForm from './../forms/AnaluzerForm'
import Results from './../components/Results'
import { onFormSubmit } from './../store/analyzer/actions'
import { Redirect } from 'react-router';
import { Link } from "react-router-dom";

class Analuzer extends React.Component {
  submit = (values, dispatch) => { dispatch(onFormSubmit(values)) }

  render() {
    if (this.props.currentUser) {
      return (
        <div>
          <AnaluzerForm onSubmit={this.submit} />
          { Object.keys(this.props.results).length > 0 &&
            <Results results={this.props.results} />
          }
        </div>
        )
    } else {
      return (
        <div>
          You need to login
          <Link className='btn btn-outline-ligh' to="/login">Login</Link>
        </div>
        )
    }
  }
}

export default Analuzer;


