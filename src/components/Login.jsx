import React from 'react';
import LoginForm from './../forms/LoginForm'
import { onFormSubmit } from './../store/login/actions'
import { Redirect } from 'react-router';

class Login extends React.Component {
  submit = (values, dispatch) => { dispatch(onFormSubmit(values)) }

  render() {
    if (this.props.currentUser) {
      return (<Redirect to={'/'} />)
    } else {
      return <LoginForm onSubmit={this.submit} />
    }
  }
}

export default Login;


