import React from 'react';
import { Link } from "react-router-dom";
import { logout } from './../store/loginInfo/actions'

class LoginInfo extends React.Component {
  constructor(){
    super();
  }
  onLogout = () => {
    const { logout } = this.props.actions;
    logout()
  }

  render() {

    return (
      <div>
        {(() => {
              if (this.props.currentUser){
                  return (
                    <span>
                      <span>
                        Login as:
                      </span>
                      <span>
                        {this.props.currentUser}
                      </span>
                    </span>
                  )
              }

              return null;
            })()}

        {(this.props.currentUser ?
          <button onClick={this.onLogout}> Logout</button> :
          <Link className='btn btn-outline-ligh' to="/login">Login</Link>
        )}

      </div>
    )
  }
}

export default LoginInfo;
