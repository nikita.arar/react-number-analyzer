import {
  LOGIN_CHANGE_EMAIL,
  LOGIN_CHANGE_PASSWORD,
  LOGIN_FAILED,
  LOGIN_SUCCESS
} from "./actions";

const defaultState = {
  email: '',
  password: '',
  currentUser: ''
}

export const loginReducer = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN_CHANGE_EMAIL:
      return { ...state, email: action.payload };

    case LOGIN_CHANGE_PASSWORD:
      return { ...state, password: action.payload };

    case LOGIN_SUCCESS:
      return { ...state, currentUser: action.payload };

    case LOGIN_FAILED:
      return { ...state, currentUser: null };

    default: return state;
  }
}
