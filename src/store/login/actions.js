import axios from "axios";

export const LOGIN_CHANGE_EMAIL = 'LOGIN_CHANGE_EMAIL'
export const LOGIN_CHANGE_PASSWORD = 'LOGIN_CHANGE_PASSWORD'
export const LOGIN_ON_SUBMIT = 'LOGIN_ON_SUBMIT'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILED = 'LOGIN_FAILED'

export const setEmail = (email) => ({
  type: LOGIN_CHANGE_EMAIL,
  payload: email
})

export const setPassword = (password) => ({
  type: LOGIN_CHANGE_PASSWORD,
  payload: password
})

export const loginSuccess = user => ({
    type: 'LOGIN_SUCCESS',
    payload: user
})

export const loginFailed = () => ({
    type: 'LOGIN_FAILED'
})

export function onFormSubmit(values) {
  return dispatch => {
    return axios.post('http://localhost:3001/api/user_token', {
      auth: {
        login: values.login,
        password: values.password
      }
    })
    .then(function (response) {
      localStorage.setItem('user', response.data.jwt)
      dispatch(loginSuccess(values.login))
    })
    .catch(function (error) {
      dispatch(loginFailed())
    })
  }
}

export function getAuth() {
  return function (dispatch) {
    return axios.get('http://localhost:3001/api/users/show', {
      headers: { "Authorization": "Bearer " + localStorage.getItem('user') }
    }).then(function (response) {
      dispatch(loginSuccess(response.data.login))
    }).catch(function (error) {
      dispatch(loginFailed())
      localStorage.removeItem('user')
    })
  }
}
