import { combineReducers } from 'redux'

import { loginReducer } from './login/reducers'
import { loginInfoReducer } from './loginInfo/reducers'
import { analyzerReducer } from './analyzer/reducers'
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  login: loginReducer,
  loginInfo: loginInfoReducer,
  analyzer: analyzerReducer,
  form: formReducer
});
