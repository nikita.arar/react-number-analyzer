import axios from "axios";

export const SET_RESULTS = 'SET_RESULTS'

export const setResults = data => ({
    type: 'SET_RESULTS',
    payload: data
})

export function onFormSubmit(values) {
  return dispatch => {
    return  axios({
      method: 'post',
      url: 'http://localhost:3001/api/analyze',
      data: values,
      headers: {
        Authorization: "Bearer " + localStorage.getItem('user')
      }
    })
    .then(function (response) {
      dispatch(setResults(response.data))
    })
    .catch(function (error) {
    })
  }
}

