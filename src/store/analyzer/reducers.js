import {
  SET_RESULTS,
} from "./actions";

const defaultState = {
  results: {}
}

export const analyzerReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_RESULTS:
      return { ...state, results: action.payload };

    default: return state;
  }
}
