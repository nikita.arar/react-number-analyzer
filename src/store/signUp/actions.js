import axios from "axios";
import { onFormSubmit as login } from './../login/actions'

export const SIGN_UP_CHANGE_EMAIL = 'SIGN_UP_CHANGE_EMAIL'
export const SIGN_UP_CHANGE_PASSWORD = 'SIGN_UP_CHANGE_PASSWORD'
export const SIGN_UP_CHANGE_PASSWORD_CONFIRMATION = 'SIGN_UP_CHANGE_PASSWORD_CONFIRMATION'
export const SIGN_UP_SUBMIT_START = 'SIGN_UP_SUBMIT_START'
export const SIGN_UP_SUBMIT_SUCCESS = 'SIGN_UP_SUBMIT_SUCCESS'

export const setEmail = (email) => ({
  type: SIGN_UP_CHANGE_EMAIL,
  payload: email
})

export const setPassword = (password) => ({
  type: SIGN_UP_CHANGE_PASSWORD,
  payload: password
})

export const setPasswordConfirmation = (passwordConfirmation) => ({
  type: SIGN_UP_CHANGE_PASSWORD_CONFIRMATION,
  payload: passwordConfirmation
})

export const signUpSuccess = values => ({
  type: 'SIGN_UP_SUBMIT_SUCCESS',
  payload: values
})
// export function onFormSubmit(values) {
//   return {
//     type: SIGN_UP_SUBMIT_START,
//     payload:
//       axios.post('http://localhost:3001/api/users', {
//         login: values.login,
//         password: values.password,
//         password_confirmation: values.password_confirmation
//       })
//       .then(function (response) {
//         if (response.status == 201) {
//           login(values)
//         } else {
//           console.log(response);
//         }
//       })
//       .catch(function (error) {
//         console.log(error);
//       })
//   }
// }

export function onFormSubmit(values) {
  return function (dispatch) {
    return  axios.post('http://localhost:3001/api/users', {
        login: values.login,
        password: values.password,
        password_confirmation: values.password_confirmation
      })
      .then(function (response) {
        if (response.status == 201) {
          dispatch(login(values))
        } else {
          console.log(response);
        }
      })
      .catch(function (error) {
        console.log(error);
      })
  }
}

// export function asffasfas(values) {

//   // Thunk middleware знает, как обращаться с функциями.
//   // Он передает метод dispatch в качестве аргумента функции,
//   // т.к. это позволяет отправить экшен самостоятельно.

//   return function (dispatch) {

//     // Первая отправка: состояние приложения обновлено,
//     // чтобы сообщить, что запускается вызов API.

//     dispatch(requestPosts(subreddit))

//     // Функция, вызываемая Thunk middleware, может возвращать значение,
//     // которое передается как возвращаемое значение метода dispatch.

//     // В этом случае мы возвращаем promise.
//     // Thunk middleware не требует этого, но это удобно для нас.

//     return fetch(`https://www.reddit.com/r/${subreddit}.json`)
//       .then(
//         response => response.json(),
//         // Не используйте catch, потому что это также                 // перехватит любые ошибки в диспетчеризации и                  // в результате рендеринга, что приведет к                         // циклу ошибок «Unexpected batch number».
//         // https://github.com/facebook/react/issues/6895
//         error => console.log('An error occurred.', error)
//       )
//       .then(json =>
//         // Мы можем вызывать dispatch много раз!
//         // Здесь мы обновляем состояние приложения с результатами вызова API.

//         dispatch(receivePosts(subreddit, json))
//       )
//   }
// }
