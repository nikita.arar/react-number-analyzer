import {
  SIGN_UP_CHANGE_EMAIL,
  SIGN_UP_CHANGE_PASSWORD,
  SIGN_UP_CHANGE_PASSWORD_CONFIRMATION
} from "./actions";

const defaultState = {
  email: '',
  password: '',
  passwordConfirmation: ''
}

export const signUpReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SIGN_UP_CHANGE_EMAIL:
      return { ...state, email: action.payload };

    case SIGN_UP_CHANGE_PASSWORD:
      return { ...state, password: action.payload };

    case SIGN_UP_CHANGE_PASSWORD_CONFIRMATION:
      return { ...state, passwordConfirmation: action.payload };

    case SIGN_UP_SUBMIT_SUCCESS:
      return { ...state };

    default: return state;
  }
}
