import {
  CLEAR_USER,
} from "./actions";

const defaultState = {
  currentUser: ''
}

export const loginInfoReducer = (state = defaultState, action) => {
  switch (action.type) {
    case CLEAR_USER:
      return { ...state, currentUser: null };

    default: return state;
  }
}
