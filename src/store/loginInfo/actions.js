export const CLEAR_USER = 'CLEAR_USER'

export const clearUser = () => ({
    type: 'CLEAR_USER'
})

export function logout() {

  return dispatch => {
    localStorage.removeItem('user')
    dispatch(clearUser())
  }
}
