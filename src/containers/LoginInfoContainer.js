import React from 'react'

import LoginInfo from './../components/LoginInfo'
import { logout } from './../store/loginInfo/actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';

class LoginInfoContainer extends React.Component {
  render() {
    return <LoginInfo {...this.props} />
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.login.currentUser
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      logout
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginInfoContainer)
