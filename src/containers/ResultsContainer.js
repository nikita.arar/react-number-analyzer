import React from 'react'

import Results from './../components/Results'
import { connect } from 'react-redux'

class ResultsContainer extends React.Component {
  render() {
    return <Results {...this.props} />
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.login.currentUser
  }
}

export default connect(mapStateToProps)(ResultsContainer)
