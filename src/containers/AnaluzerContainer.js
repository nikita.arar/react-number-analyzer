import React from 'react'
import Analuzer from './../components/Analuzer'
import { connect } from 'react-redux'

class AnaluzerContainer extends React.Component {
  render() {
    return (
      <Analuzer {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.login.currentUser,
    results: state.analyzer.results
  }
}

export default connect(mapStateToProps)(AnaluzerContainer)
