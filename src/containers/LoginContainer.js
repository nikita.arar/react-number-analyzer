import React from 'react'
import Login from './../components/Login'
import { connect } from 'react-redux'

class LoginContainer extends React.Component {
  render() {
    return (
      <Login {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.login.currentUser
  }
}

export default connect(mapStateToProps)(LoginContainer)
