import React from 'react'
import SignUp from './../components/SignUp'
import { connect } from 'react-redux'

import { setEmail, setPassword, setPasswordConfirmation } from './../store/signUp/actions'

class SignUpContainer extends React.Component {
  render() {
    return (
      <SignUp {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.login.currentUser
  }
}

const mapDispatchToProps = {
  setEmail, setPassword, setPasswordConfirmation
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpContainer)
