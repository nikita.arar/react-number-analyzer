import React from 'react';
import './App.css';
import { Switch, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'
import { getAuth } from './store/login/actions'

import Login from './containers/LoginContainer';
import SignUp from './containers/SignUpContainer';
import Results from './containers/ResultsContainer';
import LoginInfo from './containers/LoginInfoContainer';
import Analuzer from './containers/AnaluzerContainer';

class App extends React.Component {
  componentDidMount() {
    this.props.getAuth()
  }

  render() {
    return (
      <div className="App">
        <header>
          <nav className="navbar bg-primary">
            <div className="navbar-brand">
              <Link className='brandLink' to="/">Numbers Analuzer</Link>
            </div>
            <div className="loginPanel">
              <LoginInfo />
            </div>
          </nav>
        </header>
        <div className="container">
          <Switch>
            <Route exact path="/">
              <Analuzer />
            </Route>
            <Route path="/sign_up">
              <SignUp />
           </Route>
           <Route path="/login">
              <Login />
           </Route>
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isReady: state.isReady,
})

const mapDispatchToProps = {
  getAuth,
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
