import React from 'react'
import { Field, reduxForm } from 'redux-form'

class AnalyzerForm extends React.Component {
  render(){

    const { handleSubmit } = this.props;

    return (
        <form onSubmit={handleSubmit}>
          <h2> Enter Arrayy </h2>
          <div className="form-group">
            <label>Numbers 1
              <Field
                name="numbers_1"
                className='form-control'
                component="input"
                type="text"
              />
            </label>
          </div>
          <div className="form-group">
            <label>Numbers 2
              <Field
                name="numbers_2"
                className='form-control'
                component="input"
                type="text"
              />
            </label>
          </div>
          <input className='btn btn-success' type="submit" name="Analyze"/>
      </form>
      )
  }
}

AnalyzerForm = reduxForm({
  form: 'analyze'
})(AnalyzerForm)

export default AnalyzerForm
