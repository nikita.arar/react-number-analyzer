import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { Link } from "react-router-dom";


class SignUpForm extends React.Component {
  render(){

    const { handleSubmit } = this.props;

    return (
        <form onSubmit={handleSubmit}>
          <h2> Sign Up Form</h2>
          <div className="form-group">
            <label>Login
              <Field
                name="login"
                className='form-control'
                component="input"
                type="text"
                placeholder="login"
              />
            </label>
          </div>
          <div className="form-group">
            <label>Password
              <Field
                name="password"
                className='form-control'
                component="input"
                type="password"
                placeholder="password"
              />
            </label>
          </div>
          <div className="form-group">
            <label>Password confirmation
              <Field
                name="password_confirmation"
                className='form-control'
                component="input"
                type="password"
                placeholder="password"
              />
            </label>
          </div>
          <input className='btn btn-success' type="submit" name="Sign Up"/>
          <Link className='btn btn-primary' to="/login">Login</Link>
      </form>
      )
  }
}

SignUpForm = reduxForm({
  form: 'signUp'
})(SignUpForm)

export default SignUpForm
