import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { Link } from "react-router-dom";

class LoginForm extends React.Component {
  render(){

    const { handleSubmit } = this.props;

    return (
        <form onSubmit={handleSubmit}>
          <h2> Login Form</h2>
          <div className="form-group">
            <label>Login
              <Field
                name="login"
                className='form-control'
                component="input"
                type="text"
                placeholder="login"
              />
            </label>
          </div>
          <div className="form-group">
            <label>Password
              <Field
                name="password"
                className='form-control'
                component="input"
                type="password"
                placeholder="password"
              />
            </label>
          </div>
          <input className='btn btn-success' type="submit" name="Login"/>
          <Link className='btn btn-primary' to="/sign_up">Sign Up</Link>
      </form>
      )
  }
}

LoginForm = reduxForm({
  form: 'login'
})(LoginForm)

export default LoginForm
